package auto3dtrackingsystem.service;

import auto3dtrackingsystem.domain.Claim;
import auto3dtrackingsystem.domain.Status;
import auto3dtrackingsystem.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.persistence.PostUpdate;

@Component
public class StatusListener {

    @Autowired
    private MailSender mailSender;

    @PostUpdate
    public void setUpdatedOn(Claim claim) {
        if (claim.getStatus() == Status.CREATED) {
            User user = claim.getUser();
            String message = String.format(
                    "Добрый день, %s! " +
                            "Ваша заявка СОЗДАНА. Все подробности можете узнать в личном кабинете.", user.getName()
            );
            mailSender.send(user.getEmail(), "Update Status your claim in A3D", message);
        }
        if (claim.getStatus() == Status.COMPLETED) {
            User user = claim.getUser();
            String message = String.format(
                    "Добрый день, %s! " +
                            "Ваша заявка ГОТОВА. Все подробности можете узнать в личном кабинете.", user.getName()
            );
            mailSender.send(user.getEmail(), "Update Status your claim in A3D", message);
        }
        if (claim.getStatus() == Status.IN_WORK) {
            User user = claim.getUser();
            String message = String.format(
                    "Добрый день, %s! " +
                            "Ваша заявка В РАБОТЕ. Все подробности можете узнать в личном кабинете.", user.getName()
            );
            mailSender.send(user.getEmail(), "Update Status your claim in A3D", message);
        }
        if (claim.getStatus() == Status.CANCELED) {
            User user = claim.getUser();
            String message = String.format(
                    "Добрый день, %s! " +
                            "Ваша заявка ОТКЛОНЕНА. Все подробности можете узнать в личном кабинете.", user.getName()
            );
            mailSender.send(user.getEmail(), "Update Status your claim in A3D", message);
        }
    }


}
