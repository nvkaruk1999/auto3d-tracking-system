package auto3dtrackingsystem.controllers;

import auto3dtrackingsystem.domain.Claim;

/**
 * Created by pro on 01.03.2020.
 */
public class ClaimAndUserId {
    private Claim claim;
    private String userId;

    public ClaimAndUserId(Claim claim, String userId) {
        this.claim = claim;
        this.userId = userId;
    }

    public ClaimAndUserId() {
    }

    public Claim getClaim() {
        return claim;
    }

    public void setClaim(Claim claim) {
        this.claim = claim;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
