package auto3dtrackingsystem.domain;

public final class Views {
    public interface Id {}

    public interface IdName extends Id {}

    public interface FullClaims extends Id {}
}
