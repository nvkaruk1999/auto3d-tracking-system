package auto3dtrackingsystem.repository;

import auto3dtrackingsystem.domain.Claim;
import auto3dtrackingsystem.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ClaimRepository extends JpaRepository<Claim, Long> {
    Iterable<Claim> findAllByUser(User user);
    Optional<Claim> findById(Long id);
}
